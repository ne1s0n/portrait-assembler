Quick script to assemble small images in batches of nine (3x3 grid) into
bigger images.

The idea is to start from a collection of images (characters, landscapes, anything) 
and create several "container" images ready to be printed on an A4 or letter 
size.

It's an R script so you'll need R. Also, it uses EBImages package for image
manipulation. You can install it with:

source("http://bioconductor.org/biocLite.R")
biocLite("EBImage")

The bad news is I'll probably not mantain the code.
The good news is that it's so simple you'll be easily able to hack it.